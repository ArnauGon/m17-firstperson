using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestEnemicEric : MonoBehaviour
{
    enum estatsEnemic { Seguir, Atacar };


    public ScriptableEnemic scrEne;

    string nom;
    float hp;
    int dmg;
    float atkspd;
    float movspd;

    estatsEnemic estatActual;

    public GameObject personatge;
    private GameObject triggerDetectar;

    public GameEventInteger OnPlayerHit;

    void Start()
    {

        this.nom = scrEne.nom;
        this.hp = scrEne.hpMax;
        this.dmg = scrEne.dmg;
        this.atkspd = scrEne.atkspd;
        this.movspd = scrEne.movspd;


        setRigidbodyState(true);
        setColliderState(false);

        triggerDetectar = transform.GetChild(0).gameObject;
    }



    public void morir()
    {
        Destroy(gameObject, 3f);

        GetComponent<Animator>().enabled = false;

        setRigidbodyState(false);
        setColliderState(true);

    }

    void setRigidbodyState(bool state)
    {

        Rigidbody[] rigids = GetComponentsInChildren<Rigidbody>();

        foreach (Rigidbody rigidbody in rigids)
        {
            rigidbody.isKinematic = state;
        }

        GetComponent<Rigidbody>().isKinematic = !state;

    }

    void setColliderState(bool state)
    {

        Collider[] colliders = GetComponentsInChildren<Collider>();

        foreach (Collider collider in colliders)
        {
            collider.enabled = state;
        }

        GetComponent<Collider>().enabled = !state;

    }


    public void recibirdmg(int dmg)
    {
        this.hp -= dmg;
        print("He rebut " + dmg + " punts de dany. Tinc aquesta vida : " + this.hp);
        if (this.hp <= 0)
        {
            morir();
            print("HE MORT");
        }
    }

    // Update is called once per frame
    void Update()
    {

        FunEstatEnemic();
        triggerDetectar = this.gameObject.transform.GetChild(0).gameObject;
        Debug.Log("El CHILD ES " + triggerDetectar.name);

    }


    void FunEstatEnemic()
    {
        if (estatActual == estatsEnemic.Atacar)
        {
            Debug.Log("T'ATACO");
            StartCoroutine(atacarCooldown());
            //ataca
        }
        if (estatActual == estatsEnemic.Seguir)
        {
            Debug.Log("SEGUINT");
            this.transform.GetChild(0).gameObject.SetActive(true);

        }

    }

    IEnumerator atacarCooldown()
    {

        OnPlayerHit.Raise(this.dmg);
        yield return new WaitForSeconds(2f);
    }


    void rebremal()
    {

        this.hp -= 1;//enemic mal no implementat

    }



    void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Player")
        {
            estatActual = estatsEnemic.Atacar;
        }
        else
        {
            estatActual = estatsEnemic.Seguir;
        }

    }
}
