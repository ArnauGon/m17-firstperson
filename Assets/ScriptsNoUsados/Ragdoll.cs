using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ragdoll : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

        setRigidbodyState(true);
        setColliderState(false);

    }

  
    public void morir()
    {
        Destroy(gameObject, 3f);

        GetComponent<Animator>().enabled = false;

        setRigidbodyState(false);
        setColliderState(true);

    }

    void setRigidbodyState(bool state)
    {

        Rigidbody[] rigids = GetComponentsInChildren<Rigidbody>();

        foreach(Rigidbody rigidbody in rigids)
        {
            rigidbody.isKinematic = state;
        }

        GetComponent<Rigidbody>().isKinematic = !state;

    }

    void setColliderState(bool state)
    {

        Collider[] colliders = GetComponentsInChildren<Collider>();

        foreach(Collider collider in colliders)
        {
            collider.enabled = state;
        }

        GetComponent<Collider>().enabled = !state;

    }

   


}
