using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonatgeController : MonoBehaviour
{

    CharacterController controller;

    public float speed = 10f;

    public float gravity = -9.8f;

    public float jumpheight = 3;

    public bool isGrounded;

    float alturaPsj;

    Vector3 velocitat;

    public GameEvent eventdia;

    public GameObject camera;

    public LayerMask IgnoreLayer;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        controller = GetComponent<CharacterController>();
        alturaPsj = this.transform.localScale.x;
    }


    void Update()
    {
        //MOVIMENT
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;

        controller.Move(move * speed * Time.deltaTime);

        //RAYCAST TERRA
        isGrounded = Physics.Raycast(transform.position + transform.up * 0.3f, Vector3.down, 10f);

        if (isGrounded && Input.GetKeyDown(KeyCode.Space))
        {
            velocitat.y = jumpheight;
        }

        velocitat.y += gravity * Time.deltaTime;

        controller.Move(velocitat * Time.deltaTime);

    }

}
