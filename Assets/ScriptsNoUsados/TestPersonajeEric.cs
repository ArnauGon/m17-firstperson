using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestPersonajeEric : MonoBehaviour
{
    [SerializeField]
    Camera m_fpsCamera;

    [SerializeField]
    GameObject bala;

    float cadencia = 0;

    ArmaScriptable armaActual;

    [SerializeField]
    ArmaScriptable[] armes;

    public GameObject[] modeloArmasArray;

    public GameObject modeloActivo;

    public ArmaScriptable ArmaActual { get => armaActual; set => armaActual = value; }

    public LayerMask IgnoreLayer;

    public bool pucDisparar;

    private void Start()
    {
        modeloActivo = modeloArmasArray[0];
        LoadArma(0);
        pucDisparar = true;
    }

    private void Update()
    {
        print(ArmaActual.municion);

        if (Input.GetKeyDown(KeyCode.R))
            StartCoroutine(Recargar());

        if (ArmaActual.tipoDisparo == ArmaScriptable.TipoDisparo.SPREAD)
        {
            DisparoEscopeta();
        }
        if (ArmaActual.tipoDisparo == ArmaScriptable.TipoDisparo.SEMI)
        {
            DisparoSniper();
        }
        if (ArmaActual.tipoDisparo == ArmaScriptable.TipoDisparo.AUTO)
        {
            DisparoFusil();
        }

        cadencia -= Time.deltaTime;
    }

    private void LoadArma(int arma)
    {
        ArmaActual = armes[arma];
        ArmaActual.municion = ArmaActual.maxMunicion;
        ArmaActual.WeaponModel = armes[arma].WeaponModel;

        modeloActivo.SetActive(false);
        modeloActivo = modeloArmasArray[arma];
        modeloActivo.SetActive(true);

        //print("HE CANVIAT A L'ARMA : " + ArmaActual.nom);
    }

    public void CanviArmaGUI(int a)
    {
        LoadArma(a);
    }

    private void DisparoEscopeta()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (ArmaActual.municion > 0)
            {
                if (cadencia <= 0)
                {
                    RaycastHit hit;
                    if (Physics.Raycast(m_fpsCamera.transform.position, m_fpsCamera.transform.forward, out hit, IgnoreLayer, IgnoreLayer))
                    {
                        Debug.DrawLine(m_fpsCamera.transform.position, hit.point, Color.blue, 5f);

                    }

                    for (int i = 0; i < 7; i++)
                    {

                        Vector3 dispersionVertical = m_fpsCamera.transform.up * UnityEngine.Random.Range(-0.1f, 0.1f);
                        Vector3 dispersionHotizontal = m_fpsCamera.transform.right * UnityEngine.Random.Range(-0.1f, 0.1f);

                        RaycastHit hit1;
                        if (Physics.Raycast(m_fpsCamera.transform.position, m_fpsCamera.transform.forward + dispersionVertical + dispersionHotizontal, out hit1, IgnoreLayer))
                        {

                            TestEnemicEric enemic = hit.transform.GetComponent<TestEnemicEric>();

                            if(enemic != null)
                            {
                                enemic.morir();
                            }

                            Debug.DrawLine(m_fpsCamera.transform.position, hit1.point, Color.blue, 5f);

                            cadencia = ArmaActual.cadencia;

                        }

                    }
                    ArmaActual.municion--;

                }
            }
            else
            {
                StartCoroutine(Recargar());
            }

        }

    }


    private void DisparoSniper()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (ArmaActual.municion > 0)
            {
                if (pucDisparar)
                {

                    RaycastHit hit;
                    if (Physics.Raycast(m_fpsCamera.transform.position, m_fpsCamera.transform.forward, out hit, IgnoreLayer))
                    {
                        Debug.DrawLine(m_fpsCamera.transform.position, hit.point, Color.blue, 5f);
                    }

                    StartCoroutine(AbleToShoot(armaActual.cadencia));


                    ArmaActual.municion--;
                }
            }
            else
            {
                StartCoroutine(Recargar());
            }
        }


    }

    private void DisparoFusil()
    {
        if (Input.GetMouseButton(0))
        {
            if (ArmaActual.municion > 0)
            {
                if (pucDisparar)
                {

                    RaycastHit hit;
                    if (Physics.Raycast(m_fpsCamera.transform.position, m_fpsCamera.transform.forward, out hit, IgnoreLayer))
                    {
                        Debug.DrawLine(m_fpsCamera.transform.position, hit.point, Color.blue, 5f);
                    }

                    StartCoroutine(AbleToShoot(armaActual.cadencia));


                    ArmaActual.municion--;
                }
            }
            else
            {
                StartCoroutine(Recargar());
            }
        }

    }


    IEnumerator AbleToShoot(float cadencia)
    {
        pucDisparar = false;
        yield return new WaitForSeconds(cadencia);
        pucDisparar = true;
    }

    IEnumerator Recargar()
    {
        if (ArmaActual.municion < ArmaActual.maxMunicion)
        {
            yield return new WaitForSeconds(ArmaActual.reloadTime);
            ArmaActual.municion = ArmaActual.maxMunicion;
        }
    }

}
