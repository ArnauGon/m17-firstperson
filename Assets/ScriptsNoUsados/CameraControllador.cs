using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControllador : MonoBehaviour
{

    public float sensibilitatMouse = 80f;

    public Transform player;

    float xRotation = 0;
    float yRotation = 0;


    // Update is called once per frame
    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X") * sensibilitatMouse * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * sensibilitatMouse * Time.deltaTime;

        yRotation += mouseX;
        //un + enves d'un - per invertir camera
        xRotation += mouseY;

        xRotation = Mathf.Clamp(xRotation, -90, 90);

        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);

        player.Rotate(Vector3.up * mouseX);

    }
}
