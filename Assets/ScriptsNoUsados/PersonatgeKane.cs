using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonatgeKane : MonoBehaviour
{

    [SerializeField]
    private Camera m_fpsCamera;

    [SerializeField]
    private GameObject puntabala, bala, mirilla;

    [SerializeField]
    private float m_aimSensitivity = 3.0f;
    [SerializeField]
    private float m_Speed = 3.0f;

    [SerializeField]
    private bool m_invertY , salt;

    float x, y;

    [SerializeField]
    Rigidbody rb;

    Vector3 moveDirection;

    public ArmaScriptable arma;


    private int hp;
    private int maxhp = 100;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();
        Cursor.visible = false;
        this.hp = maxhp;
    }

    void Update()
    {
        m_inputs();
    }

    //per fisiques
    private void FixedUpdate()
    {
        m_moviment();
    }

    void m_moviment()
    {
        Vector3 moviment = new Vector3(Input.GetAxis("Horizontal"), 0 ,Input.GetAxis("Vertical")).normalized;
        rb.MovePosition(transform.position + moviment * Time.deltaTime * m_Speed);

        rb.AddForce(moveDirection.normalized * m_Speed, ForceMode.Force);
    }

    void m_inputs()
    {
        x = Input.GetAxisRaw("Horizontal");
        y = Input.GetAxisRaw("Vertical");
       
        moveDirection = transform.forward * y + transform.right * x;

        //salt
        if (Input.GetKeyDown("space") && salt)
        {
            rb.AddForce(transform.up * 10, ForceMode.Impulse);
            salt = false;
        }

        //sprint
        if (Input.GetKey(KeyCode.LeftShift))
        {
            m_Speed = 10f;
        }
        else
        {
            m_Speed = 5f;
        }

        //ajupit
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            Onajupit();
        }
        if (Input.GetKeyUp(KeyCode.LeftControl)){
            OffAjupit();
        }

        
        if (Input.GetAxis("Mouse X") != 0)
        {
            transform.Rotate(Vector3.up * m_aimSensitivity * Input.GetAxis("Mouse X"));
        }
        if (Input.GetAxis("Mouse Y") != 0)
        {
            m_fpsCamera.transform.Rotate(Vector3.right * m_aimSensitivity * Input.GetAxis("Mouse Y") * (m_invertY ? 1 : -1));
           // arma.transform.Rotate(Vector3.right * m_aimSensitivity * Input.GetAxis("Mouse Y") * (m_invertY ? 1 : -1));
        }

        //mirilla
        if (Input.GetMouseButton(1))
        {
            RaycastHit hit;
            Physics.Raycast(m_fpsCamera.transform.position, m_fpsCamera.transform.forward, out hit);

            if (hit.distance - 0.5 > 0.75f || hit.collider == null)
            {
                Debug.DrawLine(m_fpsCamera.transform.position, hit.point, Color.red, 5f);
                print(hit.distance - 0.5f);
                mirilla.SetActive(true);
                m_fpsCamera.fieldOfView = 50f;
            }
        }
        if (Input.GetMouseButtonUp(1))
        {
            m_fpsCamera.fieldOfView = 90f;
            mirilla.SetActive(false);
        }
    }

    void Onajupit()
    {
        this.transform.localScale = new Vector3(this.transform.localScale.x, 0.5f, this.transform.localScale.z);
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y - 0.5f, this.transform.position.z);
    }

    void OffAjupit()
    {
        this.transform.localScale = new Vector3(this.transform.localScale.x, 1f, this.transform.localScale.z);
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 0.5f, this.transform.position.z);

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "suelo")
        {
            salt = true;
        }
    }

    public void RecieveDamage(int dmg) {
        print("putita"+dmg);
        this.hp -= dmg;
        if(this.hp < 0)
        {
            Destroy(this.gameObject);
        }
    }

}
