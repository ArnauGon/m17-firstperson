using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonatgeEric : MonoBehaviour
{
    [SerializeField]
    private float m_speed = 3.0f;

    [SerializeField]
    private float m_aimSensitivity = 3.0f;

    [SerializeField]
    private Camera m_fpsCamera;

    [SerializeField]
    private bool m_invertY;

    public GameObject arma;
    public GameObject puntabala;
    public GameObject bala;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 groundPlaneMovement = Vector3.zero;
        groundPlaneMovement.x += transform.forward.x * Input.GetAxis("Vertical");
        groundPlaneMovement.z += transform.forward.z * Input.GetAxis("Vertical");

        groundPlaneMovement.x += transform.right.x * Input.GetAxis("Horizontal");
        groundPlaneMovement.z += transform.right.z * Input.GetAxis("Horizontal");

        GetComponent<Rigidbody>().velocity = groundPlaneMovement.normalized * m_speed;

        if (Input.GetAxis("Mouse X") != 0)
        {
            transform.Rotate(Vector3.up * m_aimSensitivity * Input.GetAxis("Mouse X"));
        }
        if (Input.GetAxis("Mouse Y") != 0)
        {
            m_fpsCamera.transform.Rotate(Vector3.right * m_aimSensitivity * Input.GetAxis("Mouse Y") * (m_invertY ? 1 : -1));
            arma.transform.Rotate(Vector3.right * m_aimSensitivity * Input.GetAxis("Mouse Y") * (m_invertY ? 1 : -1));
        }

        if (Input.GetMouseButton(0))
        {
            RaycastHit hit;
            if (Physics.Raycast(m_fpsCamera.transform.position, m_fpsCamera.transform.forward, out hit))
            {
                TestEnemicEric enemic = hit.transform.GetComponent<TestEnemicEric>();

                if (enemic != null)
                {
                    enemic.morir();
                }
                Debug.DrawLine(m_fpsCamera.transform.position, hit.point, Color.blue, 5f);
                //Instantiate(bala, hit.point, Quaternion.identity);
            }

        }



    }
}
