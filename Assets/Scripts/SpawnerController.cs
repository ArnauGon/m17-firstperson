using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerController : MonoBehaviour
{
    public GameObject enemic;
    public int xPos, zPos, yPos;
    public int contadorEnemics;

    void Start()
    {
        StartCoroutine(spawnEnemics());
    }

    IEnumerator spawnEnemics()
    {

        while (true) { 
        xPos = Random.Range(-40, 45);
        zPos = Random.Range(-50, 55);
        yPos = Random.Range(1, 1);
        Instantiate(enemic, new Vector3(xPos, yPos, zPos), Quaternion.identity);
        yield return new WaitForSeconds(4f);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
