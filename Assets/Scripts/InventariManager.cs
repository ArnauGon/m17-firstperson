using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InventariManager : MonoBehaviour
{
    [SerializeField]
    GameObject[] sprites;

    [SerializeField]
    Color selectedColor, unelectedColor;

    [SerializeField]
    GameEventInteger onArmaChanged;

    public Transform posicioArma;
    public GameObject personatge;

    [SerializeField] 
    TextMeshProUGUI textVida, textMunicio;

    [SerializeField]
    PersonajeArnau psjArnau;

    [SerializeField]
    ArmaScript armaScript;

    [SerializeField]
    Image barravida;

    private void Awake()
    {
       // for (int i = 0; i < armes.Length; i++)
       // {
            //Instantiate(armes[i], posicioArma);
      //  }
    }

    void Start()
    {
        canvicolor(0);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            canvicolor(0);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            canvicolor(1);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            canvicolor(2);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            canvicolor(3);
        }

        textVida.GetComponent<TMPro.TextMeshProUGUI>().text = "Vida: " + psjArnau.Hp;
        textMunicio.GetComponent<TMPro.TextMeshProUGUI>().text = "Muncion: " + armaScript.ArmaActual.municion + "/" + armaScript.ArmaActual.maxMunicion;


        if (Input.GetKeyDown(KeyCode.Y))
        {
            psjArnau.Hp -= 10;
            Debug.Log(psjArnau.Hp);
            barravida.fillAmount = psjArnau.Hp / 100;
        }
        baixarVidaPsj();

    }

    //S'ha de fer amb eventos
    void canvicolor(int boto)
    {
        for(int i = 0; i < sprites.Length; i++)
        {
            if (i == boto) { 
                sprites[i].GetComponent<Image>().color = selectedColor;
                onArmaChanged.Raise(i);
                //Debug.Log("S'ha activat l'arma" + armes[i].ToString());
            }
            else {
                sprites[i].GetComponent<Image>().color = unelectedColor;
                //Debug.Log("S'ha desactivat l'arma" + armes[i].ToString());

            }
        }
    }


    public void baixarVidaPsj()
    {
        barravida.fillAmount = psjArnau.Hp / 100;
    }

}
