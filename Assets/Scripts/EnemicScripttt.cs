using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemicScripttt : MonoBehaviour
{
    enum estatsEnemic { Seguir, Atacar };


    public ScriptableEnemic scrEne;

    string nom;
    float hp;
    int dmg;
    float atkspd;
    float movspd;

    estatsEnemic estatActual;
    public bool pucatacar;

    public Transform personatge;
    private GameObject triggerDetectar;

    public GameEventInteger OnPlayerHit;


    public float Hp { get => hp; set => hp = value; }

    void Start()
    {
        this.pucatacar = true;
        this.nom = scrEne.nom;
        this.Hp = scrEne.hpMax;
        this.dmg = scrEne.dmg;
        this.atkspd = scrEne.atkspd;
        this.movspd = scrEne.movspd;
        setRigidbodyState(true);
        setColliderState(false);

    }

    void Update()
    {
       // personatge = personatge.transform.position;
    }

    //Ragdoll
    void setRigidbodyState(bool state)
    {

        Rigidbody[] rigids = GetComponentsInChildren<Rigidbody>();

        foreach (Rigidbody rigidbody in rigids)
        {
            rigidbody.isKinematic = state;
        }

        GetComponent<Rigidbody>().isKinematic = !state;

    }

    void setColliderState(bool state)
    {

        Collider[] colliders = GetComponentsInChildren<Collider>();

        foreach (Collider collider in colliders)
        {
            collider.enabled = state;
        }

        GetComponent<Collider>().enabled = !state;

    }


    public void Recibirdmg(int dmg)
    {
        this.Hp -= dmg;
        print("He rebut " + dmg + " punts de dany. Tinc aquesta vida : " + this.Hp);

        if (this.Hp <= 0) {
            print("M'HE MORT");
            Destroy(gameObject, 3f);
            GetComponent<Animator>().enabled = false;
            setRigidbodyState(false);
            setColliderState(true);
            //.GetComponent<SpawnerController>().contadorEnemics--;
        }
    }

    IEnumerator atacarCooldown()
    {
        pucatacar = false;
        print(this.dmg + "AQUEST ES EL MAL QUE HAURIA DE FER");
        OnPlayerHit.Raise(this.dmg);
        yield return new WaitForSeconds(2f);
        pucatacar = true;
    }


    void OnCollisionEnter(Collision collision)
    {
        print("he colisionat amb algo");
        if (collision.transform.tag == "Player" && pucatacar)
        {
            print("He tocat un player, et faig mal");
            StartCoroutine(atacarCooldown());
            estatActual = estatsEnemic.Atacar;
        }
        else
        {
            estatActual = estatsEnemic.Seguir;
        }

    }



}