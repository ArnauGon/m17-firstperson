using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemicMovi : MonoBehaviour
{

    [SerializeField]
    private GameObject destination;

    private NavMeshAgent agent;

    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        destination.transform.gameObject.GetComponent<PersonajeArnau>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!agent.hasPath || !agent.pathPending)
        {
            agent.SetDestination(destination.transform.position);
        }
    }
}
