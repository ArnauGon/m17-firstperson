using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ArmaScriptable : ScriptableObject
{
    [SerializeField]
    Sprite sprite;

    public string nom;
    public int damage;
    public int nbalas;
    public float cadencia;
    public int maxMunicion;
    public int municion;
    public float reloadTime;
    public TipoDisparo tipoDisparo;

    public GameObject WeaponModel;


    public enum TipoDisparo{

        AUTO,SEMI,SPREAD, MELEE

}

}
