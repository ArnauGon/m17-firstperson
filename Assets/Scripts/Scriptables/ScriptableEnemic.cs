using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ScriptableEnemic : ScriptableObject
{
    [SerializeField]
    public string nom;
    [SerializeField]
    public int hpMax;
    [SerializeField]
    public int dmg;
    [SerializeField]
    public int atkspd;
    [SerializeField]
    public int movspd;



}
