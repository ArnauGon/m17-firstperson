using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class InventariManager : MonoBehaviour
{
    [SerializeField]
    GameObject[] sprites;

    [SerializeField]
    Color selectedColor;

    [SerializeField]
    Color unelectedColor;

    [SerializeField]
    GameEventInteger onArmaChanged;

    public Transform posicioArma;

    public GameObject personatge;

    private void Awake()
    {
       // for (int i = 0; i < armes.Length; i++)
       // {
            //Instantiate(armes[i], posicioArma);
      //  }
    }

    void Start()
    {
      
        canvicolor(0);

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            canvicolor(0);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            canvicolor(1);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            canvicolor(2);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            canvicolor(3);
        }
    }

    //S'ha de fer amb eventos
    void canvicolor(int boto)
    {
        for(int i = 0; i < sprites.Length; i++)
        {
            if (i == boto) { 
                sprites[i].GetComponent<Image>().color = selectedColor;
                onArmaChanged.Raise(i);
                //Debug.Log("S'ha activat l'arma" + armes[i].ToString());
            }
            else {
                sprites[i].GetComponent<Image>().color = unelectedColor;
                //Debug.Log("S'ha desactivat l'arma" + armes[i].ToString());

            }
        }
    }


}
