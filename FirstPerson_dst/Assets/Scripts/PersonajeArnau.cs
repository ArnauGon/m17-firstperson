using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonajeArnau : MonoBehaviour
{
    [SerializeField]
    private float m_speed = 3.0f;

    [SerializeField]
    private float m_aimSensitivity = 3.0f;

    [SerializeField]
    private Camera m_fpsCamera;

    [SerializeField]
    private bool m_invertY;

    public ArmaScriptable arma;

    public GameObject bala;

    public GameObject[] armes;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {

        Move();
        //DisparoEscopeta();
        PosarMira();

    }

    private void Move()
    {
        Vector3 groundPlaneMovement = Vector3.zero;
        groundPlaneMovement.x += transform.forward.x * Input.GetAxis("Vertical");
        groundPlaneMovement.z += transform.forward.z * Input.GetAxis("Vertical");

        groundPlaneMovement.x += transform.right.x * Input.GetAxis("Horizontal");
        groundPlaneMovement.z += transform.right.z * Input.GetAxis("Horizontal");

        GetComponent<Rigidbody>().velocity = groundPlaneMovement.normalized * m_speed;

        if (Input.GetAxis("Mouse X") != 0)
        {
            transform.Rotate(Vector3.up * m_aimSensitivity * Input.GetAxis("Mouse X"));
        }
        if (Input.GetAxis("Mouse Y") != 0)
        {
            m_fpsCamera.transform.Rotate(Vector3.right * m_aimSensitivity * Input.GetAxis("Mouse Y") * (m_invertY ? 1 : -1));
            arma.WeaponModel.transform.Rotate(Vector3.right * m_aimSensitivity * Input.GetAxis("Mouse Y") * (m_invertY ? 1 : -1));
        }
    }



    void PosarMira()
    {
        if (Input.GetMouseButton(1))
        {
            RaycastHit hit;
            if (Physics.Raycast(m_fpsCamera.transform.position, m_fpsCamera.transform.forward, out hit))
            {
                Debug.DrawLine(m_fpsCamera.transform.position, hit.point, Color.red, 5f);
                print(hit.distance - 0.5f);
            }
            if (hit.distance > 1)
            {
                m_fpsCamera.fieldOfView = 50f;
            }
        }
        if (Input.GetMouseButtonUp(1))
        {
            m_fpsCamera.fieldOfView = 60f;

        }
    }
}
