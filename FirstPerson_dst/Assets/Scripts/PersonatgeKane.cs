using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonatgeKane : MonoBehaviour
{


    [SerializeField]
    private float m_aimSensitivity = 3.0f;

    [SerializeField]
    private Camera m_fpsCamera;

    [SerializeField]
    private bool m_invertY;


    //yo

    public GameObject arma;
    public GameObject puntabala;
    public GameObject bala;
    public GameObject mirilla;

    [SerializeField]
    Rigidbody rb;

    [SerializeField]
    private float m_Speed = 3.0f;


    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        Vector3 m_Input = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        rb.MovePosition(transform.position + m_Input * Time.deltaTime * m_Speed);

    }

    // Update is called once per frame
    void Update()
    {
        /*
         Vector3 groundPlaneMovement = Vector3.zero;
         groundPlaneMovement.x += transform.forward.x * Input.GetAxis("Vertical");
         groundPlaneMovement.z += transform.forward.z * Input.GetAxis("Vertical");

         groundPlaneMovement.x += transform.right.x * Input.GetAxis("Horizontal");
         groundPlaneMovement.z += transform.right.z * Input.GetAxis("Horizontal");
         */

        //GetComponent<Rigidbody>().velocity = groundPlaneMovement.normalized * m_speed;

        if (Input.GetKeyDown("space")){

            rb.AddForce(transform.up * 5, ForceMode.Impulse);
        }
        if (Input.GetKey("left shift")){

            m_Speed = 20;
        }
        else
        {
            m_Speed = 3f;
        }




        if (Input.GetAxis("Mouse X") != 0)
        {
            transform.Rotate(Vector3.up * m_aimSensitivity * Input.GetAxis("Mouse X"));
        }
        if (Input.GetAxis("Mouse Y") != 0)
        {
            m_fpsCamera.transform.Rotate(Vector3.right * m_aimSensitivity * Input.GetAxis("Mouse Y") * (m_invertY ? 1 : -1));
            arma.transform.Rotate(Vector3.right * m_aimSensitivity * Input.GetAxis("Mouse Y") * (m_invertY ? 1 : -1));
        }
        

        posarmira();
    }






    void posarmira()
    {
        if (Input.GetMouseButton(1))
        {
            RaycastHit hit;
            Physics.Raycast(m_fpsCamera.transform.position, m_fpsCamera.transform.forward, out hit);

            if (hit.distance - 0.5 > 0.75f || hit.collider == null)
            {
                Debug.DrawLine(m_fpsCamera.transform.position, hit.point, Color.red, 5f);
                print(hit.distance-0.5f);
                mirilla.SetActive(true);
                m_fpsCamera.fieldOfView = 50f;
            }
        }
        if (Input.GetMouseButtonUp(1))
        {
            m_fpsCamera.fieldOfView = 90f;
            mirilla.SetActive(false);
        }
    }


}
