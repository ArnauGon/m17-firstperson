using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemicMovi : MonoBehaviour
{

    [SerializeField]
    private GameObject destination;

    private NavMeshAgent agent;
    // Start is called before the first frame update
    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!agent.hasPath || !agent.pathPending)
        {
            agent.SetDestination(destination.transform.position);

        }
           
    }
}
