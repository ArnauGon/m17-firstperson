using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ScriptableEnemic : ScriptableObject
{
    [SerializeField]
    public string nom;
    [SerializeField]
    public float hpMax;
    [SerializeField]
    public float dmg;
    [SerializeField]
    public float atkspd;
    [SerializeField]
    public float movspd;



}
