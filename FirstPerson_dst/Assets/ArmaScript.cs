using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmaScript : MonoBehaviour
{
    [SerializeField]
    Camera m_fpsCamera;

    [SerializeField]
    GameObject bala;

    float cadencia = 0;

    ArmaScriptable armaActual;

    [SerializeField]
    ArmaScriptable[] armes;

    private void Start()
    {
        LoadArma(0);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
            StartCoroutine(Recargar());

        if (armaActual.tipoDisparo == ArmaScriptable.TipoDisparo.SPREAD) {
        DisparoEscopeta();
        }
        if (armaActual.tipoDisparo == ArmaScriptable.TipoDisparo.SEMI)
        {
        DisparoSniper();
        }
        if (armaActual.tipoDisparo == ArmaScriptable.TipoDisparo.AUTO)
        {
        DisparoFusil();
        }

        cadencia -= Time.deltaTime;
        //print(cadencia);
    }

    private void LoadArma(int arma)
    {
        armaActual = armes[arma];
        armaActual.municion = armaActual.maxMunicion;
        armaActual.WeaponModel = armes[arma].WeaponModel;


        /*GameObject clone = armaActual.WeaponModel;
        Instantiate(clone);
        clone.transform.position = this.transform.position;*/
        //canvi de malla
        //animaci� de canvi d'arma
        print("HE CANVIAT A L'ARMA : "+armaActual.nom);
    }

    public void CanviArmaGUI(int a)
    {
        LoadArma(a);
    }

    private void DisparoEscopeta()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (armaActual.municion > 0)
            {
                if (cadencia <= 0)
                {
                    RaycastHit hit;
                    if (Physics.Raycast(m_fpsCamera.transform.position, m_fpsCamera.transform.forward, out hit))
                        Debug.DrawLine(m_fpsCamera.transform.position, hit.point, Color.blue, 5f);

                    for (int i = 0; i < 7; i++)
                    {

                        Vector3 dispersionVertical = m_fpsCamera.transform.up * UnityEngine.Random.Range(-0.1f, 0.1f);
                        Vector3 dispersionHotizontal = m_fpsCamera.transform.right * UnityEngine.Random.Range(-0.1f, 0.1f);
                        print("VERTICAL : " + dispersionVertical);
                        print("HORIZONTAL : " + dispersionHotizontal);

                        RaycastHit hit1;
                        if (Physics.Raycast(m_fpsCamera.transform.position, m_fpsCamera.transform.forward + dispersionVertical + dispersionHotizontal, out hit1))
                        {

                            Debug.DrawLine(m_fpsCamera.transform.position, hit1.point, Color.blue, 5f);
                            GameObject go = Instantiate(bala);
                            go.transform.position = hit1.point;

                            cadencia = armaActual.cadencia;
                            Destroy(go, 3f);
                        }

                    }
                    armaActual.municion--;

                }
            }
            else {
                StartCoroutine(Recargar());
            }

        }

    }


    private void DisparoSniper()
    {
         if (Input.GetMouseButtonDown(0))
         {
            if (armaActual.municion > 0)
            {
                if (cadencia <= 0)
                {


                RaycastHit hit;
                if (Physics.Raycast(m_fpsCamera.transform.position, m_fpsCamera.transform.forward, out hit))
                    Debug.DrawLine(m_fpsCamera.transform.position, hit.point, Color.blue, 5f);

                GameObject go = Instantiate(bala);
                go.transform.position = hit.point;

                cadencia = armaActual.cadencia;
                Destroy(go, 3f);


                }
            }
            else
            {
                Recargar();
            }
        }

    }

    private void DisparoFusil()
    {
        if (Input.GetMouseButton(0))
        {
            if (cadencia <= 0)
            {


                RaycastHit hit;
                if (Physics.Raycast(m_fpsCamera.transform.position, m_fpsCamera.transform.forward, out hit))
                    Debug.DrawLine(m_fpsCamera.transform.position, hit.point, Color.blue, 5f);

                GameObject go = Instantiate(bala);
                go.transform.position = hit.point;

                cadencia = armaActual.cadencia;
                Destroy(go, 3f);


            }
        }

    }
    IEnumerator Recargar()
    {
        if (armaActual.municion < armaActual.maxMunicion) {
            yield return new WaitForSeconds(armaActual.reloadTime);
            armaActual.municion = armaActual.maxMunicion;
        }
    }

}
